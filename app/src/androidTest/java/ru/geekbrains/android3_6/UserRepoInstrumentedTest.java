package ru.geekbrains.android3_6;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.TestObserver;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import ru.geekbrains.android3_6.di.DaggerTestComponent;
import ru.geekbrains.android3_6.di.TestComponent;
import ru.geekbrains.android3_6.di.modules.ApiModule;
import ru.geekbrains.android3_6.model.cache.ICache;
import ru.geekbrains.android3_6.model.entity.Repository;
import ru.geekbrains.android3_6.model.entity.User;
import ru.geekbrains.android3_6.model.repo.UsersRepo;

import static junit.framework.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class UserRepoInstrumentedTest
{
    private static MockWebServer webServer;

    @Inject
    UsersRepo usersRepo;

    @Inject
    ICache iCache;

    @BeforeClass
    public static void setupClass() throws IOException
    {
        webServer = new MockWebServer();
        webServer.start();
    }

    @AfterClass
    public static void tearDownClass() throws IOException
    {
        webServer.shutdown();
    }

    @Before
    public void setup()
    {
        TestComponent component = DaggerTestComponent
                .builder()
                .apiModule(new ApiModule(){
                    @Override
                       public String endpoint()
                    {
                        return webServer.url("/").toString();
                    }
                })
                .build();

        component.inject(this);
    }

    @After
    public void tearDown()
    {

    }

    @Test
    public void getUser()
    {
        webServer.enqueue(createUserResponse("somelogin", "someurl"));
        TestObserver<User> observer = new TestObserver<>();
        usersRepo.getUser("somelogin").subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertValueCount(1);
        assertEquals(observer.values().get(0).getLogin(), "somelogin");
        assertEquals(observer.values().get(0).getAvatarUrl(), "someurl");
    }

    private MockResponse createUserResponse(String login, String avatar)
    {
        String body = "{\"login\":\"" + login + "\", \"avatar_url\":\"" + avatar +"\"}";
        return new MockResponse()
                .addHeader("Content-Type", "application/json; charset=utf-8")
                .addHeader("Cache-Control", "no-cache")
                .setBody(body);
    }

    @Test
    public void putUser(){
        String login = "user1";
        String avatarUrl = "avatar1";
        User user = new User(login,avatarUrl);
        iCache.putUser(user);

        TestObserver<User> observer = new TestObserver<>();
        iCache.getUser(login).subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertValueCount(1);
        assertEquals(observer.values().get(0).getLogin(), login);
        assertEquals(observer.values().get(0).getAvatarUrl(), avatarUrl);
    }

    @Test
    public void putUserRepos(){

        putUserReposNewUser();
        putUserReposOldUser();

    }

    public void putUserReposNewUser(){
        String login = "user1";
        String avatarUrl = "avatar1";
        User user = new User(login,avatarUrl);
        // iCache.putUser(user);

        String id = "1";
        String name = "test";
        ArrayList<Repository> repos = new ArrayList<>();
        repos.add(new Repository(id,name));
        iCache.putUserRepos(user,repos);

        TestObserver<List<Repository>> observer = new TestObserver<>();
        iCache.getUserRepos(user).subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertValueCount(1);
        assertEquals(observer.values().get(0).get(0).getId(), id);
        assertEquals(observer.values().get(0).get(0).getName(), name);
    }

    public void putUserReposOldUser(){
        String login = "user1";
        String avatarUrl = "avatar1";
        User user = new User(login,avatarUrl);
        iCache.putUser(user);

        String id = "1";
        String name = "test";
        ArrayList<Repository> repos = new ArrayList<>();
        repos.add(new Repository(id,name));
        iCache.putUserRepos(user,repos);

        TestObserver<List<Repository>> observer = new TestObserver<>();
        iCache.getUserRepos(user).subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertValueCount(1);
        assertEquals(observer.values().get(0).get(0).getId(), id);
        assertEquals(observer.values().get(0).get(0).getName(), name);
    }


    @Test
    public void getUserFromCache(){
        String login = "user2";
        String avatarUrl = "avatar2";
        User user = new User(login,avatarUrl);

        TestObserver<User> observer = new TestObserver<>();
        iCache.getUser(login).subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertValueCount(0);
    }


    @Test
    public void getUserReposFromCache(){

        getUserReposNewUser();
        getUserReposExistUser();

    }


    public void getUserReposNewUser(){
        String login = "user11";
        String avatarUrl = "avatar1";
        User user = new User(login,avatarUrl);

        TestObserver<List<Repository>> observer = new TestObserver<>();
        iCache.getUserRepos(user).subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertValueCount(0);
    }


    public void getUserReposExistUser(){
        String login = "user12";
        String avatarUrl = "avatar1";
        User user = new User(login,avatarUrl);

        String id = "2";
        String name = "test";
        ArrayList<Repository> repos = new ArrayList<>();
        repos.add(new Repository(id,name));
        iCache.putUserRepos(user,repos);

        TestObserver<List<Repository>> observer = new TestObserver<>();
        iCache.getUserRepos(user).subscribe(observer);
        observer.awaitTerminalEvent();
        observer.assertValueCount(1);
    }
}
