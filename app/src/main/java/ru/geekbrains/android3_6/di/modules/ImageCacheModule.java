package ru.geekbrains.android3_6.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.geekbrains.android3_6.model.cache.ImageCache;

@Module
public class ImageCacheModule {

    @Singleton
    @Provides
    public ImageCache getImageCache() {
        return new ImageCache();
    }

}
